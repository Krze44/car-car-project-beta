import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO

AUTOMOBILE_API = "http://inventory-api:8000/api/automobiles/"


def poll():
    while True:
        print("Sales poller polling for data")
        try:
            response = requests.get(AUTOMOBILE_API)
            data = json.loads(response.content)
            autos = data["autos"]

            for auto in autos:
                AutomobileVO.objects.update_or_create(vin=auto["vin"])

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
