# CarCar

Team:

* Justin Brayshaw - automobile service microservice
* Santiago Bothe - automobile sales microservice

## Design

View the full flowchart at:
https://gitlab.com/Krze44/car-car-project-beta/-/blob/main/README.md

The Backend of our project Uses three bounded contexts: Inventory, Sales, and Service. Sales and Service both use pollers to get Automobile data from Inventory. This allows them to create AutomobileVO (value Objects).

```mermaid
flowchart LR
    subgraph inventory Bounded Context
    inventory_rest ---> idb[(models)]
    end
    subgraph Sales Bounded Context
    sales_rest --> salesdb[(models)]
    sales_poller --> sales_rest
    sales_rest --> sales_poller
    end
    subgraph Service Bounded Context
    service_rest --> servivedb[(models)]
    service_poller --> service_rest
    service_rest --> service_poller
    end
    sales_poller  <-.-> inventory_rest
    service_poller <-.-> inventory_rest
```

The front end makes API calls from the App.js file. The data that is returned from these calls is used to populate the front end. Additional "post" and "get" requests are made following the successful completion of a form.

```mermaid
flowchart LR
    subgraph Backend
    inventory_rest
    service_rest
    sales_rest
    end
    subgraph Front End
    app.js
    end
    app.js <--> inventory_rest
    app.js <--> service_rest
    app.js <--> sales_rest
```

## Instructions -
  1. go to https://gitlab.com/Krze44/car-car-project-beta and fork the project
  2. click clone, and clone with https
  3. go to your directory of choice to clone the project, via cd commands in your terminal of choice
  4. run $ git clone https://gitlab.com/your-user-name/your-project-name-here
  5. cd into the car-car-project-beta directory
  6. open docker desktop
  7. run $ docker volume create beta-data
  8. run $ docker-compose build
  9. run $ docker-compose up
  10. left click on the react-1 container and wait for the logs to say "starting development server..." followed by "Compiled successfully!"
  11. once the react container is fully up and running, open your browser of choice and input the following in the the search bar "http://localhost:3000/" which will bring you to the homepage
  12. once on the homepage, the navbar will have 5 links, the CarCar logo, the Home link, and 3 drop down menus with the links for each service, one for the inventory service, the automobile-services service, and the automobile-sales service
  13. you will notice upon clicking a drop down that each feature is sectioned by a bar between the links, creating a manufacturer and adding one etc... clicking on any of the links excluding the dropdown themselves will take you to correct page
  14. if you would like to navigate to the inventory without using the links you can do so in the following format, http://localhost:3000/inventory/manufacturers/list or (/new), http://localhost:3000/inventory/models/list or (/new), http://localhost:3000/inventory/models/list or (/new)
  15. if you would like to navigate to the automobile-service without using the links you can do so in the following format, http://localhost:3000/automobile-service/technicians/list or (/new), http://localhost:3000/automobile-service/appointments/list or (/new), or http://localhost:3000/automobile-service/history
  16. if you would like to navigate to the automobile-sales without using the links you can do so in the following format, localhost:3000/automobile-sales/salespeople/list or (/new), http://localhost:3000/automobile-sales/customers/list or (/new), http://localhost:3000/automobile-sales/sales/list or (/new), or http://localhost:3000/automobile-sales/history

## Service microservice
* Justin Brayshaw

    * Ports - 8080:8000
    * Base URL - http://localhost:8080/api/
    ## Models and CRUD ##
    * The Service Microservice is compirsed of 2 object models and one Value Object Model -
        # Technician -
        * requires, first name, last name, and employee id, id can consist of letters and numbers
            # CRUD & CRUD URLS -

            * GET - http://localhost:8080/api/technicians/
                returns a list of technicians

            * POST - http://localhost:8080/api/technicians/
                Example JSON format -

                {
                    "first_name": "first",
                    "last_name": "last",
                    "employee_id": 2
                }
                returns the first name, last name, employee id, and the django id

            * DELETE - http://localhost:8080/api/technicians/<int:employee_id>/ -
                returns "deleted": true, if successfully deleted, and displays an error if the technician with
                provided employee id doesn't exist

        # Appointment -
        * requires, a date time object, reason for appointment, status of appointment (canceled, finished, or blank), a VIN (vehicle identification number), customer name, and technician appointed to the appointment
            # CRUD & CRUD URLS -

            * GET - http://localhost:8080/api/appointments/
                returns a list of appointments

            * POST - http://localhost:8080/api/appointments/
            Example JSON format -

                {
                    "date_time": "2023-04-20T14:39:00.000Z",
                    "reason": "reason code 2",
                    "vin":"2222",
                    "customer": "Warren Longmire",
                    "technician": 2
                }
                returns - the id of the appointment, a date-time string, the reason for the appointment, an empty status string, a VIN, the Technician model, including the Tech's django ID, and a customer name

            * DELETE - http://localhost:8080/api/appointments/<int:id>/
                returns "deleted": true, if successfully deleted, and displays an error if an appointment with that id doesnt exist
            * PUT - "http://localhost:8080/api/appointments/<int:id>/finish/" & "http://localhost:8080/api/appointments/<int:id>/cancel/"
              returns the entire appointment object, with the status updated to either "finished" or "canceled"

        # AutomobileVO (Value Object) -
        * this is the value object/value object model, it takes in a vin, import_href, and the sold-status (True or False),
        * because its a value object it has no api end-points

    # Integration -

    # POLLER -
        * Every 60s it makes a GET request using the request module to - "http://inventory-api:8000/api/automobiles/"

        * the back-end endpoint for this is - "http://localhost:8100/api/automobiles/"

        * this returns a list of JSON Automobile objects, from which we loop over each object in the list, and use the update_or_create() method to create a new or update and existing AutomobileVO (AutomobileValueObject) with the vin field being the vin inside the current automobile object, the import href, being the href field inside the automobile object, and the sold field, being the sold field inside the automobile object.



#### Sales microservice ####
* Santiago Bothe

  * Ports - 8090:8000
  * Base URL - http://localhost:8090/api/
  ## Models and CRUD ##
  The Sales Microservice is comprised of 4 objects models (One of which is a Value Object and relies on the  poller to populate its data):

    # Salesperson:
    * consists of a first name, last name, and employer id.

      # CRUD
        * GET - http://localhost:8090/api/salespeople/
          returns a list of salespeople
        * POST - http://localhost:8090/api/salespeople/
          Accepts a valid first name, last name, and employee id.
          returns a new salesperson. Such as:

          {

            "first_name": "Ricky",

            "last_name": "Gervais",

            "employee_id": "RickyG12"

          }

          The employee_id value musy be a unique string.

        * GET - http://localhost:8090/api/salespeople/(id)/
          returns a salesperson who's employee id matches the url number (id)
        * DELETE - http://localhost:8090/api/salespeople/(id)/
          returns the deleted salseperson who's employee id matches the url number (id)
        * PUT - is currently not supported

    # Customer,
    * consists of first name, last name, address, and phone number.

      # CRUD
        * GET - http://localhost:8090/api/customers/
          returns a list of cutsomers
        * POST - http://localhost:8090/api/customers/
          Accepts a valid first name, last name, address, and phone. Such as:

          {

            "first_name" : "Ricky",

            "last_name" : "Bobby",

            "address" : "5432 Spider Monkey Drive",

            "phone_number" : "7206975678"

          }

          The phone_number value must be 10 digits
          This is to refelct standard American numbers

          returns a new customer
        * GET - http://localhost:8090/api/customers/(id)/
          returns a customer who's id matches the url number (id)
        * DELETE - http://localhost:8090/api/customers/(id)/
          returns the deleted customer who's id matches the url number (id)
        * PUT - is currently not supported

    # Sale,
    * consists of price, customer, salesperson, and automobile.
    This is the only model that uses a forign key and it does so to the Salesperson model, the Customer model, and the AutomobileVO ojects.
    I also created a specific Decimal encoder to handle the price values.

      # CRUD
        * GET - http://localhost:8090/api/sales/
          returns a list of sales
        * POST - http://localhost:8090/api/sales/
          Accepts a valid AutomobileVO Vin, salesperson employee id,
          customer id, and price. Such as:

          {

            "automobile": "1C3CC5FB2AN120174",

            "salesperson": 8,

            "customer": 2,

            "price": 65000.00

          }

          Price accepts a decimal value but does not accept numbers with commas.

          returns a new Sale
        * GET - http://localhost:8090/api/sales/(id)/
          returns a sale matching the url (id)
        * DELETE - http://localhost:8090/api/sales/(id)/
          returns the deleted sale which has a matcing id to the url (id)
        * PUT - is currently not supported

    # AutomobileVO (Value Object),
    * consists of a vin number.
    This is a value object and is never referenced directly outside of the sales microservice and the poller.

## integration ##

  ## poller ##
  * Every 60 seconds the poller makes an API call using the requests module to the inventory api. More specifically it makess a "get" request to
    - http://inventory-api:8000/api/automobiles/.

    Which is the same as http://localhost:8100/api/automobiles/ on insomnia.

    This request returns a list of Automobile objects. Which the poller then loops over and uses to either create or update the AutomobileVO objects in the sales_rest models.

    AutomobileVO objects have a vin number which directly corresponds to that of the inventory Automobile objects vin numbers. This way we can creata a sale based on the Automobile Models without having to directly call he inventory_rest api from the sales_rest api.

    # Poller Problems
      * If someone were to create an automobile instance and try to create a sale instance before the Poller had a chance to call the inventory_rest api and update the AutomobileVO data, The request to create that sale would fail.

      The Poller works well to get all of the data from the inventory but a queue might be a better option if the data were to get larger.

  # Front end
  On the fron end, the file App.js makes a get request for all of our lists of Salesperson, Customer, and Sale objects from the sales_rest api. Additionally, it also retireves a list of, Manufacturers, Models, and Automobiles from the inventory_rest api.

  Imediatly following the automobile get request, the function GetAutomobileData creates another list by filtering out cars which have not yet been sold.

  We made this decision so we only have to render the lists when we load the page, or when we create a new instance of a model. For example, When a new instance of a Salesperson is made, The handleSubmit functions calls the function GetSalespeopleData(This function is passed down from App.js).
  This function does two main things:
    - creates a new Salesperson instance through the Salesperson Post api
    - retrieves the entire list of Salesperson instances through the Salesperson Get api (the one without the id).

  * The sales Micro-service is integrated in the fron end in the "automobile-sales" portion of the navigation bar. There our front end uses the information passed down as props to render lists and forms. Once the forms are submitted, the same process as above occurs. This is true for all parts of the Automobile-sales portion of the front-end.

  * The Django-Cors_headers needed to be configured since it was already one of the required applications. The correct additions following the latest reccomendations from the specific pip library were added in settings under "installed apps" and under "Middleware"
  [Django-cors-headers](https://pypi.org/project/django-cors-headers/)

#### Inventory Microservice ####
* Both

  * Ports - 8100:8000
  * Base URL - http://localhost:8100/api/
  ## Models and CRUD ##
  The Inventory Microservice is comprised of 3 objects models:

    # Manufacturer:
    * Consists of a unique name

      # CRUD
        * GET - http://localhost:8100/api/manufacturers/
          returns a list of Manufacturers
        * POST - http://localhost:8100/api/manufacturers/
          Accepts a valid unique name, such as:

          {

            "name": "Audi"

          }

          The name value musy be a unique string.

        * GET - http://localhost:8100/api/manufacturers/(id)/
          returns a manufacturer who's id matches the url number (id)
        * DELETE - http://localhost:8100/api/manufacturers/(id)/
          returns the deleted manufacturer who's id matches the url number (id)
        * PUT - Accepts a valid unique name, such as:

          {

            "name": "Audi"

          }

    # VehicleModel,
    * consists of a name, picture_url, and manufacturer(forignKey).

      # CRUD
        * GET - http://localhost:8100/api/models/
          returns a list of VehicleModels
        * POST - http://localhost:8100/api/models/ - accepts a valid name, picture_url, and manufacturer.

          {

            "name": "e-tron GT",

            "picture_url": "https://www.autotrader.
            com/wp-content/uploads/2022/08/2023-audi-e-tron-gt-front-leftjpg.jpg",

            "manufacturer_id": 6

          }

          returns a new VehicleModel

        * GET - http://localhost:8100/api/models/(id)/
          returns a VehicleModel who's id matches the url number (id)
        * DELETE - http://localhost:8100/api/models/(id)/
          returns the deleted VehicleModel who's id matches the url number (id)
        * PUT - accepts a valid picture url, such as:

          {

          "picture_url": "https://www.autotrader.com/wp-content/uploads/2022/08/2023-audi-e-tron-gt-front-leftjpg.jpg?w=1024"

          }

    # Automobile,
    * consists of color, year, vin, model(ForignKey) and sold boolean.

      # CRUD
        * GET - http://localhost:8100/api/automobiles/
          returns a list of Automobiles
        * POST - http://localhost:8100/api/automobiles/
          Accepts a valid color, vin, year,and model id. Such as:

          {

          "color": "green",

          "year": 2012,

          "vin": "1C3CC5FB2AN120176",

          "model_id": 2

          }

          returns a new Sale
        * GET - http://localhost:8100/api/automobiles/(VIN)
          returns an automobile matching the url (VIN)
        * DELETE - http://localhost:8100/api/automobiles/(VIN)
          returns the deleted sale which has a matcing id to the url (id)
        * PUT - http://localhost:8100/api/automobiles/(VIN) accepts a valid color, year, and sold boolean.

          {

          "color": "red",

          "year": 2012,

          "sold": true

          }

        * Upon Creating a new sale, the automobiled's sold status will get updated through this api call
