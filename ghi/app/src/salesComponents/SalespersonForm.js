import { useState } from "react";
import Spinner from "./Spinner";

const SalespersonForm = ({GetSalespeopleData, salespeople}) => {

  const [loading, setLoading] = useState(false)
  const [invalidId, setInvalidId] = useState(false)
  const [success, setSuccess] = useState(false)
  const [first, setFirst] = useState("");
  const [last, setLast] = useState("");
  const [employeeId, setEmployeeId] = useState("");

  const handleChange = (event, callBack) => {
    const value = event.target.value;
    callBack(value);
  }

  const setToFalse = (callback) => {
    callback(false)
  }

  const handleMessages = (callBack) => {
    setTimeout(() => (setToFalse(callBack)), 10000)
  }

  const checkUniqueId = () => {
    for (let i of salespeople) {
      if (i.employee_id === employeeId) {
        return false;
      }
    }
    return true;
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    setLoading(true)
    const check = checkUniqueId()
    if ( check === false) {
      setLoading(false)
      setSuccess(false)
      setInvalidId(true)
      handleMessages(setInvalidId)
      return false;
    }

    const data = {};
    data.first_name = first[0].toUpperCase()+first.slice(1).toLowerCase();
    data.last_name = last[0].toUpperCase()+last.slice(1).toLowerCase();
    data.employee_id = employeeId;

    const salespeopleUrl = "http://localhost:8090/api/salespeople/"
    const FetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          "Content-Type": "application/json",
      }
    }
    const response = await fetch(salespeopleUrl, FetchConfig);
    if (response.ok) {

      GetSalespeopleData()
      setFirst("")
      setLast("")
      setEmployeeId("")
      setLoading(false)
      setInvalidId(false)
      setSuccess(true)
      handleMessages(setSuccess)

    }
  }

  return (
    <>
    {invalidId && <div className="row justify-content-center"><div className="position-absolute col-6 alert alert-danger p-2" role="alert">The Employee Id has already been taken</div></div>}
    {success && <div className="row justify-content-center"><div className="position-absolute col-6 alert alert-success p-2" role="alert">A new Salesperson has been added!</div></div>}
    <div className="row justify-content-center mt-5">
      <div className="col-6 card">
        <div className="card-body">
        <form onSubmit={handleSubmit} className="row g-3">
            <h1>Add a Salesperson</h1>
              <input required value={first} onChange={(event) => handleChange(event, setFirst)} type="text" className="form-control" id="FirstName" placeholder="First Name"/>
              <input required value={last} onChange={(event) => handleChange(event, setLast)} type="text" className="form-control" id="LastName" placeholder="Last Name"/>
              <input required value={employeeId} onChange={(event) => handleChange(event, setEmployeeId)} type="text" className="form-control" id="EmployeeId" placeholder="Employee ID"/>
              {loading ? <Spinner />:<button type="submit" className="w-25 btn btn-primary mb-3">Create</button>}
          </form>
        </div>
      </div>
    </div>
    </>
  )

}
export default SalespersonForm;
