import { useState } from "react";

function TechnicianForm({technicians, GetTechnicianData}) {
    const [firstName, setFirst] = useState("");
    const [lastName, setLast] = useState("");
    const [employeeID, setID] = useState("");

    const handleState = ({ target }, cb) => {
        const { value } = target;
        cb(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID

        const techUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json',
            },
        };
        const response = await fetch(techUrl, fetchConfig);
        if (response.ok){
            const newTechnician = await response.json();

            GetTechnicianData();
            setFirst('');
            setLast('');
            setID('');
            event.target.reset();
        }
    }
    return (
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center mb-3">Add a Technician</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="row">
                            <input required type="text" name="First Name" placeholder="First Name" className="form-control mb-3" onChange={(event) => handleState(event, setFirst)} />
                            <input placeholder="Last Name" type="text" name="Last Name" className="form-control mb-3" onChange={(event) => handleState(event, setLast)} />
                            <input placeholder="Employee ID" type="text" name="Employee ID" className="form-control mb-3" onChange={(event) => handleState(event, setID)} />
                            <button type="submit" className="w-25 btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}
export default TechnicianForm