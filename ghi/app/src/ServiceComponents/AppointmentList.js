import { useState } from "react";
function AppointmentList({appointments, GetAppointmentData}) {
    const [status, setStatus] = useState("");

    if(appointments === undefined){
        return null;
    }

    const filteredAppointments = appointments.filter(
        (appointment) => {
        return appointment.status === ""
    });


    const cancel = async (id) =>{
        const url = `http://localhost:8080/api/appointments/${id}/cancel/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({"status": "canceled"}),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok){
            GetAppointmentData();
        }
    }

    const finish = async (id) =>{
        const url = `http://localhost:8080/api/appointments/${id}/finish/`

        const fetchConfig = {
            method: "put",
            body: JSON.stringify({"status": "canceled"}),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        
        if (response.ok){
            GetAppointmentData();
        }
    }

    return(
        <>
        <h1 className="text-center">Service Appointments</h1>
        <table className="table table-bordered table-striped table-hover mt-5 text-center">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>Date & Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {filteredAppointments.map(appointment => {
                    const userTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
                    const date_time = appointment.date_time
                    const date = new Date(date_time);
                    const options = {
                        month: "numeric",
                        day: "numeric",
                        year: "numeric",
                        hour: "numeric",
                        minute: "numeric",
                        hour12: true,
                        timeZone: userTimezone,
                    };
                    const id = appointment.id
                    const formattedDate = new Intl.DateTimeFormat('en-US', options).format(date);
                    return(
                        <tr key={id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.customer}</td>
                            <td>{formattedDate}</td>
                            <td>{appointment.technician.first_name}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => cancel(id)}> Cancel </button> 
                                <button className="btn btn-success ms-3" onClick={() => finish(id)}> Finish </button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}

export default AppointmentList