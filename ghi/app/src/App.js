import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import AutomobileList from './inventoryComponents/AutomobileList';
import AutomobileForm from './inventoryComponents/AutomobileForm';
import ManufacturerList from './inventoryComponents/Manufacturers List';
import ManufacturerForm from './inventoryComponents/ManufacturerForm';
import ModelForm from './inventoryComponents/ModelForm';
import ModelList from './inventoryComponents/ModelList';

import CustomerForm from './salesComponents/CustomerForm';
import CustomerList from './salesComponents/CustomerList';
import SaleForm from './salesComponents/SaleForm';
import SaleList from './salesComponents/SaleList';
import SalespersonForm from './salesComponents/SalespersonForm';
import SalespeopleList from './salesComponents/SalespeopleList';
import SalespersonHistory from './salesComponents/SalepersonHistory';

import TechnicianList from './ServiceComponents/TechnicianList';
import TechnicianForm from './ServiceComponents/TechnicianForm';
import AppointmentList from './ServiceComponents/AppointmentList';
import AppointmentForm from './ServiceComponents/AppointmentForm';
import ServiceHistory from './ServiceComponents/ServiceHistory';

import { useState, useEffect } from 'react';

function App() {

  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [automobiles, setAutomobiles] = useState([])
  const [unsoldAutomobiles, setUnsoldAutomobiles] = useState([])
  const [salespeople, setSalespeople] = useState([])
  const [customers, setCustomers] = useState([])
  const [sales, setSales] = useState([])
  const [technicians, setTechnicians] = useState([])
  const [appointments, setAppointments] = useState([])

  const GetManufacturersData = async () => {
    const manufacturersUrl = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(manufacturersUrl);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  const GetModelsData = async () => {
    const modelsUrl = "http://localhost:8100/api/models/";
    const response = await fetch(modelsUrl);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  const GetAutomobilesData = async () => {
    const automobilesUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(automobilesUrl);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos)
      const forSale = data.autos.filter(automobile => automobile.sold === false)
      setUnsoldAutomobiles(forSale)

    }
  }

  const GetSalespeopleData = async () => {
    const salespeopleUrls = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salespeopleUrls);
    if (response.ok) {
      const data = await response.json(salespeopleUrls);
      setSalespeople(data.salespeople)
    }
  }

  const GetCustomersData = async () => {
    const customerUrl = "http://localhost:8090/api/customers/";
    const response = await fetch(customerUrl);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers)
    }
  }

  const GetSalesData = async () => {
    const salesUrl = "http://localhost:8090/api/sales/";
    const response = await fetch(salesUrl);
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    }
  }

  const GetTechnicianData = async () => {
    const technicianUrl = "http://localhost:8080/api/technicians/  ";
    const response = await fetch(technicianUrl);
    if (response.ok){
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  const GetAppointmentData = async () => {
    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const response = await fetch(appointmentUrl);
    if (response.ok){
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }

  useEffect(() => {
    GetManufacturersData()
    GetAutomobilesData()
    GetModelsData()
    GetSalespeopleData()
    GetCustomersData()
    GetSalesData()
    GetAppointmentData()
    GetTechnicianData()
  },[])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='inventory'>
            <Route path='manufacturers'>
              <Route path='list' element={<ManufacturerList manufacturers={manufacturers}/>}/>
              <Route path='new' element={<ManufacturerForm manufacturers={manufacturers} GetManufacturersData={GetManufacturersData}/>} />
            </Route>
            <Route path='models'>
              <Route path='list' element={<ModelList models={models} />} />
              <Route path='new' element={<ModelForm manufacturers={manufacturers} GetModelsData={GetModelsData} />} />
            </Route>
            <Route path='automobiles'>
              <Route path='list'  element={<AutomobileList automobiles={automobiles} />}   />
              <Route path='new'  element={<AutomobileForm models={models}  GetAutomobilesData={GetAutomobilesData} />}  />
            </Route>
          </Route>
          <Route path='automobile-service'>
            <Route path='technicians'>
              <Route path='list'  element={<TechnicianList technicians={technicians} />}  />
              <Route path='new'  element={<TechnicianForm GetTechnicianData={GetTechnicianData} />}  />
            </Route>
            <Route path='appointments'>
              <Route path='list'  element={<AppointmentList appointments={appointments} GetAppointmentData={GetAppointmentData} />}  />
              <Route path='new' element={<AppointmentForm technicians={technicians} GetAppointmentData={GetAppointmentData}  />}  />
            </Route>
            <Route path='history' element={<ServiceHistory appointments={appointments} GetAppointmentData={GetAppointmentData} />}  />
          </Route>
          <Route path='automobile-sales'>
            <Route path='salespeople'>
              <Route path='list' element={<SalespeopleList salespeople={salespeople} />}  />
              <Route path='new' element={<SalespersonForm salespeople={salespeople} GetSalespeopleData={GetSalespeopleData}/>} />
            </Route>
            <Route path='customers'>
              <Route path='list' element={<CustomerList customers={customers} />}  />
              <Route path='new' element={<CustomerForm GetCustomersData={GetCustomersData} />}  />
            </Route>
            <Route path='sales'>
              <Route path='list' element={<SaleList sales={sales} />}  />
              <Route path='new' element={<SaleForm salespeople={salespeople} customers={customers} unsoldAutomobiles={unsoldAutomobiles} GetSalesData={GetSalesData} GetAutomobilesData={GetAutomobilesData} />} />
            </Route>
            <Route path='history' element={<SalespersonHistory salespeople={salespeople} sales={sales}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
